# Dmenu-Scripts

A small collection of scripts that use dmenu.

## Installation

Move the scripts you want to use somewhere into your $PATH or other permanent
location. Then use a hotkey manager to bind a key of your choice to the
file name if in your $PATH, or its normal path if in a random folder.

## Contributing

I am open to pull requests just as long as you know [how to to write a commit
message](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).
Please report any bugs/issues you find. I am also very open to new ideas and
critique.

## Licence

zlib License

